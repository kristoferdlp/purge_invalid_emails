
package main

import (
	"time"
	"runtime"
	"sync"
	"net/smtp"
	"regexp"
	"fmt"
	"path/filepath"
	"os"
	"io/ioutil"
	"strings"
	"net"
	"strconv"
)

var regex = "^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$"
var fromEmail = "alexis@univpost.com"
var wg sync.WaitGroup

func check(str string, f_good *os.File, f_bad *os.File, tries int64) {
	if len(str) < 2 {
		return
	}
    addressToVerify := strings.TrimSpace(strings.Split(str, ",")[2])
    match, err := regexp.MatchString(regex, addressToVerify)
    if err != nil {
    	panic(err);
    }
    if !match {
    	fmt.Println("Bad Syntax")
		f_bad.WriteString(str + "\n")
    	return
    }
    domain := strings.Split(addressToVerify, "@")[1]
    nss, _ := net.LookupMX(domain)
    if len(nss) < 1 {
    	fmt.Println("No MX record for " + domain)
		f_bad.WriteString(str + "\n")
	} else {
		server := strings.TrimRight(nss[0].Host, ".") + ":25"
		hostname, _ := os.Hostname()
    	var errct int64 = 0
		for errct < tries {
			// client, err := smtp.Dial(server)
			// if err != nil {
			// 	server = strings.TrimRight(nss[0].Host, ".") + ":457"
			// 	client, err = smtp.Dial(server)
			// 	if err != nil {
			// 		server = strings.TrimRight(nss[0].Host, ".") + ":587"
			// 		client, err = smtp.Dial(server)
			// 		errct++
			// 		continue 
			// 	}
			// }
			timeout := time.Second*time.Duration(10)
			conn, err := net.DialTimeout("tcp", server, timeout)
			if err != nil {
				server = strings.TrimRight(nss[0].Host, ".") + ":457"
				conn, err = net.DialTimeout("tcp", server, timeout)
				if err != nil {
					server = strings.TrimRight(nss[0].Host, ".") + ":587"
					conn, err = net.DialTimeout("tcp", server, timeout)
					if err != nil {
						errct++
				 		continue 
					}
				}
			}
			client, err := smtp.NewClient(conn, domain)
			if err != nil {
				errct++
				continue				
			}
			err = client.Hello(hostname)
			if err != nil {
				errct++
				continue
			}
			err = client.Mail(fromEmail)
			if err != nil {
				errct++
				continue
			}
			err = client.Rcpt(addressToVerify)
			if err != nil {
				errct++
				continue
			}
			break
		}
		if errct < tries {
			f_good.WriteString(str + "\n")
			fmt.Println(addressToVerify + " SUCCESS")
		} else {
			f_bad.WriteString(str + "\n")
		}
	}

}

func processFile(foldername string, file string, tries int64) {
	fmt.Println("Starting on " + foldername + file)
	defer wg.Done()
	dat, err := ioutil.ReadFile(file)
	if err != nil {
		fmt.Println("Unable to read file " + file)
		return
	}

	// file to write to
	f_good, err := os.OpenFile(foldername + "_good/" + file[len(foldername):len(file)], os.O_WRONLY | os.O_CREATE | os.O_TRUNC, 0777)
	if err != nil {
	    panic(err)
	}
	f_bad, err := os.OpenFile(foldername + "_bad/" + file[len(foldername):len(file)], os.O_WRONLY | os.O_CREATE | os.O_TRUNC, 0777)
	if err != nil {
	    panic(err)
	}

	lines := strings.Split(string(dat), "\n")
	nlines := len(lines)
	j := 0
	for j < nlines {
		check(lines[j], f_good, f_bad, tries)
		j++
	}

	f_good.Sync()
	f_bad.Sync()

	f_good.Close()
	f_bad.Close()
}

func main() {
	
	args := os.Args[1:]
	if (len(args) < 1) {
		fmt.Println("\nUsage: ./main directory\n")
		return
	}

	// find files
	var foldername = args[0]
	files, _ := filepath.Glob(foldername + "/*")

	// use all available cpus
	cpus := runtime.NumCPU()
	runtime.GOMAXPROCS(cpus)
	fmt.Println(cpus)
	wg.Add(len(files))

	// create output directories for purged emails
	if _, err := os.Stat(foldername + "_good"); err != nil {
	    if os.IsNotExist(err) {
	        os.MkdirAll(foldername + "_good", 777)
	    } 
	}
	if _, err := os.Stat(foldername + "_bad"); err != nil {
	    if os.IsNotExist(err) {
	        os.MkdirAll(foldername + "_bad", 777)
	    } 
	}

	// loop over all files
	nfiles := len(files)
	i := 0

	var tries int64 = 30
	if len(args) > 1 {
		tries, _ = strconv.ParseInt(args[1], 10, 32)
	}

	for i < nfiles {
		go processFile(foldername, files[i], tries)
		i++
	}

	wg.Wait()
	fmt.Println("Done")

}
